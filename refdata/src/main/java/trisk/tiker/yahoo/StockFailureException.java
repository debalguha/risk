package trisk.tiker.yahoo;

/**
 * Created by debalguha on 09/04/17.
 */
public class StockFailureException extends Exception {
    private String sticker;

    public StockFailureException(String message, Throwable cause, String sticker) {
        super(message, cause);
        this.sticker = sticker;
    }

    public String getSticker() {
        return sticker;
    }
}
