package org.traderisk.ticker.config

import com.typesafe.config.ConfigFactory

import scala.io.Source

/**
  * Created by debalguha on 01/04/17.
  */
object ConfigManager extends ConfigProperties{
  val config = ConfigFactory.load
  def getStringConfig(configParam: String): String ={
    config.getString(configParam)
  }
  def loadAllStockTickers(): Seq[String] = {
    Source.fromInputStream(getClass.getResourceAsStream(stockFile)).mkString.split(",").toSeq
  }
}
