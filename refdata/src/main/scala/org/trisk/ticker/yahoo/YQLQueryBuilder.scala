package org.trisk.ticker.yahoo

import scalaj.http.{Http, HttpRequest}

/**
  * Created by debalguha on 01/04/17.
  */
class YQLQueryBuilder {
  var baseUrl = ""
  var params = collection.mutable.Map[String, String]()

  def withBaseUrl(url: String): YQLQueryBuilder = {
    this.baseUrl = url
    this
  }

  def withParam(key: String, value: String): YQLQueryBuilder = {
    this.params.put(key, value)
    this
  }

  def build(): HttpRequest = {
    var http = Http(baseUrl).proxy("localhost", 8080)
    params.foreach(tuple => {
      http = http.param(tuple._1, tuple._2)
    })
    http
  }
}
