package org.trisk.ticker.yahoo

import java.io.{File, FileWriter}
import java.util.concurrent.Callable

import org.apache.commons.io.IOUtils

/**
  * Created by debalguha on 01/04/17.
  */
case class YQLWorker(builder: YQLQueryBuilder, outputFile: File) extends Callable[Unit] {

  override def call(): Unit = {
    writeJsonToFile(builder.build().asString.body)
  }

  def writeJsonToFile(stockJson: String) = {
    val writer = new FileWriter(outputFile);
    IOUtils.write(stockJson, writer)
    writer.close
  }
}
