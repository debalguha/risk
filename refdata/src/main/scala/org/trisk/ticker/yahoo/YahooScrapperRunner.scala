package org.trisk.ticker.yahoo

import java.io.File
import java.net.URLEncoder
import java.util.concurrent.{CountDownLatch, Executors}
import java.util.concurrent.atomic.AtomicInteger

import com.typesafe.scalalogging.LazyLogging
import org.traderisk.ticker.config.ConfigManager
import trisk.tiker.yahoo.StockFailureException

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
  * Created by debalguha on 01/04/17.
  */
object YahooScrapperRunner extends App with LazyLogging{
  val allTickers = ConfigManager.loadAllStockTickers.map(aTicker => new StockTicker(aTicker, 0))
  val numJobs = new CountDownLatch(allTickers.size)
  val numThreads = ConfigManager.config.getInt("yql.num.threads")
  val failedJobs = new AtomicInteger(0)
  val passedJobs = new AtomicInteger(0)
  implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(numThreads))
  val outputFolder = new File(ConfigManager.config.getString("trisk.refdata.stock.folder"))
  logger.info(s"${numJobs.getCount} Stocks to look up")
  val tasks: Seq[Future[Unit]] = allTickers.map(aTicker => launchATask(aTicker))
  numJobs.await
  logger.info(s"All processing completed. Failed: ${failedJobs.get}, Passed: ${passedJobs.get}")
  System.exit(0)

  def launchATask(aTicker: StockTicker): Future[Unit] = {
    val future = Future {
      logger.debug(s"Starting with Ticker: ${aTicker.ticker}")
      val builder = new YQLQueryBuilder().withBaseUrl(ConfigManager.config.getString("yql.base.url"))
        .withParam(ConfigManager.config.getString("yql.params.formatting.key"), ConfigManager.config.getString("yql.params.formatting.value"))
        .withParam(ConfigManager.config.getString("yql.params.environment.key"), ConfigManager.config.getString("yql.params.environment.value"))
        .withParam(ConfigManager.config.getString("yql.params.query.key"), ConfigManager.config.getString("yql.params.query.value").replaceAll("SYM", aTicker.ticker))
      new YQLWorker(builder, new File(outputFolder, s"${aTicker.ticker}.json")).call()
      logger.debug(s"Finished with Ticker: ${aTicker.ticker}")
    }
    future.onComplete {
      case Failure(e) => {
        logger.error(s"Failed ${aTicker.ticker} ${aTicker.retry.+(1)} times!!", e);
        if(aTicker.retry == 1)
          failedJobs.incrementAndGet
        if(aTicker.retry == 5){
          numJobs.countDown
          logger.error(s"Retry exhausted for ${aTicker.ticker}. Failing for good. Download manually!!")
        }

        if(aTicker.retry < 5)
          launchATask(aTicker)
      }
      case Success(_) => logger.debug("Finished with Success!!");passedJobs.incrementAndGet;numJobs.countDown
    }
    future
  }
}
case class StockTicker(ticker: String, retry: Int)