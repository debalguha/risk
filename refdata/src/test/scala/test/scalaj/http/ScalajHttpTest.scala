package test.scalaj.http

import java.net.URLEncoder

import org.scalatest.FunSuite
import org.springframework.http.client.{ClientHttpResponse, HttpComponentsClientHttpRequestFactory}
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.web.client.{ResponseErrorHandler, RestTemplate}
import org.springframework.web.util.UriComponentsBuilder

import scala.collection.JavaConversions._
import scalaj.http.Http

/**
  * Created by debalguha on 02/04/17.
  */
class ScalajHttpTest extends FunSuite{

  val restTemplate = {
    import org.springframework.http.client.SimpleClientHttpRequestFactory
    import org.springframework.web.client.RestTemplate
    import java.net.InetSocketAddress
    import java.net.Proxy.Type
    val requestFactory = new SimpleClientHttpRequestFactory
    val proxy = new java.net.Proxy(Type.HTTP, new InetSocketAddress("localhost", 8080))
    requestFactory.setProxy(proxy)
    new RestTemplate(requestFactory)
  }

  test("Should be able to do a simple GET Request") {
    var http = Http("http://query.yahooapis.com/v1/public/yql")
      http = http.param("q", "select * from yahoo.finance.quotes where symbol in ('A')")
      .param("env", "store://datatables.org/alltableswithkeys").param("format", "json").proxy("localhost", 8080)
    //query.yahooapis.com%2Fv1%2Fpublic%2Fyql%3Fq%3Dselect%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22AAN%22)%26format%3Djson%26env%3Dstore%3A%2F%2Fdatatables.org%2Falltableswithkeys
    println(http.asString.body)
    //println(Http("http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quotes where symbol in (\"AAN\")&format=json&env=store://datatables.org/alltableswithkeys").asString.body)
    val template = restTemplate
    template.setErrorHandler(new ResponseErrorHandler {override def handleError(clientHttpResponse: ClientHttpResponse): Unit = ???

      override def hasError(clientHttpResponse: ClientHttpResponse): Boolean = {
        println(clientHttpResponse.getStatusText)
        clientHttpResponse.getRawStatusCode != 200
      }
    })
    import org.springframework.http.{HttpHeaders, MediaType}
    val headers = new HttpHeaders
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE)
    import org.springframework.http.HttpEntity
    val entity = new HttpEntity[AnyVal](headers)
    template.setMessageConverters(List(new StringHttpMessageConverter()))
    val builder = UriComponentsBuilder.fromHttpUrl("http://query.yahooapis.com/v1/public/yql")
      .queryParam("q", "select * from yahoo.finance.quotes where symbol in (\"A\")")
      .queryParam("format", "json")
      .queryParam("env", "store://datatables.org/alltableswithkeys")
    import org.springframework.http.HttpMethod
    println(template.exchange(builder.build.encode.toUri, HttpMethod.GET, entity, classOf[String]).getBody)

  }
}
