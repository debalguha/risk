package org.petalsoft.eq.risk.persistence.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by Debal on 08/05/2017.
 */
@Entity
public class HistoricalStock {
    @EmbeddedId
    private HistoricalStockPK pk;
    private double open;
    private double high;
    private double low;
    private double close;
    private long volume;
    @Column(name = "Adj_Close")
    private double adjClose;

    public HistoricalStock(){}

    public HistoricalStock(String ticker, Date businessDate, double open, double high, double low, double close, long volume, double adjClose) {
        this.pk = new HistoricalStockPK(ticker, businessDate);
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.adjClose = adjClose;
    }

    public String getTicker() {
        return pk.getTicker();
    }

    public void setTicker(String ticker) {
        this.pk.setTicker(ticker);
    }

    public Date getBusinessDate() {
        return pk.getBusinessDate();
    }

    public void setBusinessDate(Date businessDate) {
        this.pk.setBusinessDate(businessDate);
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public double getAdjClose() {
        return adjClose;
    }

    public void setAdjClose(double adjClose) {
        this.adjClose = adjClose;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoricalStock that = (HistoricalStock) o;

        return pk.equals(that.pk);
    }

    @Override
    public int hashCode() {
        return pk.hashCode();
    }

    @Override
    public String toString() {
        return "HistoricalStock{" +
                "pk=" + pk +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                ", volume=" + volume +
                ", adjClose=" + adjClose +
                '}';
    }
}
