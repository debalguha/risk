package org.petalsoft.eq.risk.persistence.repository;

import org.petalsoft.eq.risk.persistence.model.HistoricalStock;
import org.petalsoft.eq.risk.persistence.model.HistoricalStockPK;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Debal on 08/05/2017.
 */
@Repository
public interface HistoricalStockRepo extends CrudRepository<HistoricalStock, HistoricalStockPK>{}
