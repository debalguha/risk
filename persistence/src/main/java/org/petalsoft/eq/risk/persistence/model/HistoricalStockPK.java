package org.petalsoft.eq.risk.persistence.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Debal on 08/05/2017.
 */
@Embeddable
public class HistoricalStockPK implements Serializable{
    private String ticker;
    private Date businessDate;

    public HistoricalStockPK(String ticker, Date businessDate) {
        this.ticker = ticker;
        this.businessDate = businessDate;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Date getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoricalStockPK that = (HistoricalStockPK) o;

        if (!ticker.equals(that.ticker)) return false;
        return businessDate.equals(that.businessDate);
    }

    @Override
    public int hashCode() {
        int result = ticker.hashCode();
        result = 31 * result + businessDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "HistoricalStockPK{" +
                "ticker='" + ticker + '\'' +
                ", businessDate=" + businessDate +
                '}';
    }
}
