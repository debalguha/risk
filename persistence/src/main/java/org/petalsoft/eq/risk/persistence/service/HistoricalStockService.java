package org.petalsoft.eq.risk.persistence.service;

import org.petalsoft.eq.risk.persistence.model.HistoricalStock;
import org.petalsoft.eq.risk.persistence.repository.HistoricalStockRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Debal on 08/05/2017.
 */
@Service
@Transactional
public class HistoricalStockService {
    @Autowired
    private HistoricalStockRepo historicalStockRepo;

    @Transactional(rollbackFor = Throwable.class)
    public boolean addHistoricalStockData(HistoricalStock stock){
        historicalStockRepo.save(stock);
        return true;
    }
}
